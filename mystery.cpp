
// Computing 2 Lab 4 Mystery Sorting.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
using namespace std;

void print(vector A)   // Print Vector Function
{
	for (vector a : A)
		cout << a << " ";

	cout << endl;
}

void mystery1(auto& Data)  //IDK what this sort is called
{
	cout << endl << "Mystery 1" << endl << "---------------------" << endl;
	cout << "Initial vector =  ";
	print(Data);
	for (unsigned int i = 0; i < Data.size(); i++)
	{
		for (unsigned int j = 0; j < i; j++) {
			if (Data[i] < Data[j])
				swap(Data[i], Data[j]);

		}
		print(Data);
	}//end outer for (this brace is needed to include the print statement)

}

//... Other mysteries...
void selectionSort(auto& Data)    //Selection Sort Function
{
	cout << endl << "Selection Sort" << endl << "---------------------" << endl;

	int i, j, minIndex, tmp;
	cout << "Initial vector =  ";
	print(Data);

	for (i = 0; i < Data.size() - 1; i++) {
		minIndex = i;

		//find smallest in unsorted part
		for (j = i + 1; j < Data.size(); j++) {
			if (Data[j] < Data[minIndex])
				minIndex = j;
		}

		if (minIndex != i) {
			tmp = Data[i];
			Data[i] = Data[minIndex];
			Data[minIndex] = tmp;
		} //end if
		print(Data);
	} //end outer loop

} //end function

void insertionSort(auto& Data)      //Insertion Sort Function
{
	cout << endl << "Insertion Sort" << endl << "---------------------" << endl;

	cout << "Initial vector =  ";
	print(Data);
	for (int i = 1; i < Data.size(); i++) {
		int j = i;

		while (j > 0 and Data[j] < Data[j - 1]) {
			swap(Data[j], Data[j - 1]);
			j--;
		} //end while loop i.e. pass
		print(Data);
	} //end for loop

} //end function




int main()
{

	vector<int> Data = { 36, 18, 22, 30, 29, 25, 12 };

	vector<int> D1 = Data;
	vector<int> D2 = Data;
	vector<int> D3 = Data;

	mystery1(D1);
	selectionSort(D2);
	insertionSort(D3);

	system("PAUSE");   //For codeblocks

}
